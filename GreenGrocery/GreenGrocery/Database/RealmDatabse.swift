//
//  RealmDatabse.swift
//  GreenGrocery
//
//  Created by bekbossynova on 8/6/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit
import  RealmSwift


class RealmDatabase {
    static let shared: RealmDatabase = RealmDatabase()
    var notificationToken: NotificationToken?
    private init() { }
    
    deinit {
        notificationToken?.invalidate()
    }
    
}

extension RealmDatabase: CartDB {
    
    func getCartItem(usinSkuId skuId: String) -> CartItem {
        let realm = try! Realm()
        
        guard let realmObject = realm.objects(RealmCartItem.self).first(where: {$0.skuId == skuId}) else {
            return CartItem(skuId: skuId, value: 0)
        }
        return CartItem(realmCartItem: realmObject)
    }
    
    
    func updateCart(using cartItem: CartItem) -> (Bool) {
        let realm = try! Realm()
        do {
            try realm.write {
                realm.add(RealmCartItem(cartItem: cartItem), update: .all)
            }
        } catch {
            print("ERROR\(error.localizedDescription)")
            return false
        }
        
        return true
    }
    
    func delete(usingSkuId skuId: String) -> (Bool) {
        
        let realm = try! Realm ()
        
        do {
            try realm.write {
                if let realmObject = realm.objects(RealmCartItem.self).first(where: {$0.skuId == skuId}) {
                    realm.delete(realmObject)
                }
            }
            
        } catch {
            print(error.localizedDescription)
            return false
        }
        
        return true
    }
    
    
    func getCount(closure: @escaping CartCountClosure) {
        let realm = try! Realm()
        
        let results = realm.objects(RealmCartItem.self)
        
        notificationToken = results.observe({ (_) in
            closure(results.count)
        })
        
    }
    
    
    
}
