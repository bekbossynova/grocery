//
//  CartDb.swift
//  GreenGrocery
//
//  Created by bekbossynova on 8/6/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import Foundation

typealias CartCountClosure =  (Int) -> (Void)
protocol CartDB {
    func getCartItem(usinSkuId skuId: String) -> CartItem
    func updateCart(using cartItem: CartItem) -> (Bool)
    func delete(usingSkuId skuId: String) -> (Bool)
    func getCount(closure: @escaping CartCountClosure) -> (Void)
    
}
