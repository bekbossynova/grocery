//
//  ViewLoadable.swift
//  GreenGrocery
//
//  Created by bekbossynova on 8/4/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

public protocol ViewLoadable: AnyObject {
    static var nibName: String { get }
    
} 

public extension ViewLoadable {
    static func loadFromNib() -> Self {
        return loadFromNib(from: Bundle.init(for: self))
    }
    
    static func loadFromNib(from bundle: Bundle) -> Self {
        return bundle.loadNibNamed(self.nibName, owner: nil, options: nil)?.first as! Self
    }
}
