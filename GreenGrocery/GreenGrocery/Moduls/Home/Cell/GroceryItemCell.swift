//
//  GroceryItemCell.swift
//  GreenGrocery
//
//  Created by bekbossynova on 8/6/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

class GroceryItemCell: UITableViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var detailLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    
    @IBOutlet weak var addToBagController: AddBagControl!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: true)
    }
    
    func configure(usingModel viewModel: GroceryItemViewModel, addToCartClosure: @escaping BagClosure) -> Void {
        self.titleLabel.text            = viewModel.title
        self.detailLabel.text           = viewModel.details
        self.productImageView.image     = UIImage(named: viewModel.image)
        self.priceLabel.text            = viewModel.price
        
        self.addToBagController.configure(usingViewModel: viewModel.cartValue, bagClosure: addToCartClosure)
        
        self.selectionStyle = .none
    }
    
    
}
