//
//  HomeModuleBuilder.swift
//  GreenGrossery
//
//  Created by bekbossynova on 8/3/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

class HomeModuleBuilder {
    
    static func build(usingNavigationFactory factory: NavigationFactory) -> UIViewController {
        
        let storyboard      = UIStoryboard.init(name: "Home", bundle: nil)
        let view            = storyboard.instantiateViewController(identifier: "HomeViewController") as! HomeViewController
        view.title          = "Fresh Groseries"
        let router          = HomeRouter(view: view)
        let homeInteractor  = HomeInteractor(service: StubService.shared, database: RealmDatabase.shared)
        let cartInteractor  = CartInteractor(database: RealmDatabase.shared)
        
        let presenter       = HomePresenter(view: view,  router: router, useCase: (
            getGroceries: homeInteractor.getGroceries,
            addToCart:  cartInteractor.addToCart,
            getCartItem: cartInteractor.getCartItem
        ))
        
        view.presenter      = presenter
        
        return factory(view)
    }
}
