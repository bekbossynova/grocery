//
//  HomeViewController.swift
//  GreenGrossery
//
//  Created by bekbossynova on 8/3/20.
//  Copyright © 2020 bekbossynova. All rights reserved.


import UIKit

protocol HomeView: class {
    
    func updateGrocceries(groceriesList: [GroceryItemViewModel]) -> ()
}

class  HomeViewController: UIViewController {
    
    var presenter: HomePresentation?
    
    @IBOutlet weak var tableView: UITableView!
    
    private static let  groceryCellId = "groceryCellId"
    
    
    
    var dataSouce: [GroceryItemViewModel] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter?.viewDidLOad()
        self.tableView.register(UINib(nibName: "GroceryItemCell", bundle: nil), forCellReuseIdentifier: HomeViewController.groceryCellId)
        self.tableView.delegate     = self
        self.tableView.dataSource   = self
    }
}


extension HomeViewController: HomeView {
    
    func updateGrocceries(groceriesList: [GroceryItemViewModel]) {
        print("Grocery List \(groceriesList)")
        self.dataSouce = groceriesList
        self.tableView.reloadData()
    }
}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSouce.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = dataSouce[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: HomeViewController.groceryCellId, for: indexPath) as! GroceryItemCell
        
       
        cell.configure(usingModel: viewModel, addToCartClosure: { result in
            print("Cart Item addedwith sku = \(result.skuId) and quantity = \(result.stepValue)")
            
            let skuItem: SkuItem = (skuId: result.skuId, quantity: result.stepValue)
            self.presenter?.onAddToCart(skuItem: skuItem)
        })
        
        return cell
    }
    
    
}


extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
}
