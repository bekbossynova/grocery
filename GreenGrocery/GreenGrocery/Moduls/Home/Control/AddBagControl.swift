//
//  AddBagControl.swift
//  GreenGrocery
//
//  Created by bekbossynova on 8/4/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

typealias BagClosure = ((skuId: String, stepValue: Int)) -> ()

class AddBagControl: UIView, ViewLoadable {
    
    static var nibName: String = "AddBagControl"
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var stepLabel: UILabel!
    
    var closure: BagClosure?
    
    var viewModel: CartValueViewModel!  {
           didSet {
               stepLabel.text          = "\(viewModel.stepValue)"
               addButton.isHidden      = (viewModel.showStepper)
               plusButton.isHidden     = !(viewModel.showStepper)
               minusButton.isHidden    = !(viewModel.showStepper)
               stepLabel.isHidden      = !(viewModel.showStepper)

           }
       }

//    required public init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        setUp()
//    }
//
//    private func setUp() {
//        Bundle(for: type(of: self)).loadNibNamed("AddBagControl", owner: self, options: nil)
//        backgroundColor = .clear
//        addSubview(contentView)
//        contentView.frame = self.bounds
//        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
//    }

     
     func configure(usingViewModel viewModel: CartValueViewModel, bagClosure: @escaping BagClosure) -> (Void) {
         self.viewModel = viewModel
         self.addButton.setTitle(viewModel.title, for: .normal)
         self.closure = bagClosure
     }
    
    
    @IBAction func addToBag(_ sender: Any) {
        self.viewModel = self.viewModel.onAddToBag()
        self.closure?((viewModel.id, viewModel.stepValue))
      
    }
    
    
    @IBAction func incrementButton(_ sender: Any) {
        self.viewModel = self.viewModel.onIncrement()
       self.closure?((viewModel.id, viewModel.stepValue))
    }
    
    
    @IBAction func decrementButton(_ sender: Any) {
        self.viewModel = self.viewModel.onDecrement()
       self.closure?((viewModel.id, viewModel.stepValue))
    }
    
}


struct CartValueViewModel {
    let id   : String
    let title: String
    let stepValue: Int
    let showStepper: Bool
   
    
    init(id: String, stepValue: Int) {
        self.id   = id
        self.title = "ADD TO BAG"
        self.stepValue = stepValue
        self.showStepper = stepValue > 0
        
    }
}

extension CartValueViewModel {
    
    func onAddToBag() -> CartValueViewModel {
        return CartValueViewModel(id: self.id, stepValue: 1)
    }
    
    func  onIncrement() -> CartValueViewModel {
        guard self.stepValue < 10 else { return self }
        return CartValueViewModel(id: self.id , stepValue: self.stepValue + 1)
    }
    
    func onDecrement() -> CartValueViewModel {
        guard  self.stepValue > 0   else { return  self}
        return CartValueViewModel(id: self.id, stepValue: self.stepValue - 1)
    }
}
