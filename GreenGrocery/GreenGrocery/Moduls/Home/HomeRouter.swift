//
//  HomeRouter.swift
//  GreenGrossery
//
//  Created by bekbossynova on 8/3/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

//Response just navigation
protocol HomeRouting {
    
}

class HomeRouter {
    var viewController: UIViewController
    
    init(view: UIViewController) {
        self.viewController = view
    }
}

extension HomeRouter: HomeRouting {
    
}
