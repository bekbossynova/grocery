//
//  TabBarPresenter.swift
//  GreenGrocery
//
//  Created by bekbossynova on 8/10/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

protocol TabBarPresentation {
    func viewDidLoad() -> Void
}


class TabBarPresenter {
    
    weak var view: TabBarView?
    
    typealias UseCase = (
        getCartCount: ( @escaping CartCountClosure) -> Void, ()
    )
    var useCase: UseCase
    
    init(useCase: TabBarPresenter.UseCase) {
        self.useCase = useCase
    }
}

extension TabBarPresenter: TabBarPresentation {
    
    func viewDidLoad() -> Void {
            self.useCase.getCartCount {
            self.view?.updateCartCount(count: $0)
        }
    }
}
