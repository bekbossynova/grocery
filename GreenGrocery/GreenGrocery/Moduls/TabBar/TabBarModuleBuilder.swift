//
//  TabBarModuleBuilder.swift
//  GreenGrocery
//
//  Created by bekbossynova on 8/7/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

class TabBarModuleBuilder {
    
    static func build(usingSubModuls submoduls: TabBarRouter.Submodules) -> UITabBarController {
        
        let tabs                = TabBarRouter.tabs(usingSubModules: submoduls)
        
        let interactor          = CartInteractor(database: RealmDatabase.shared)
        
        let presenter           = TabBarPresenter(useCase: (
            getCartCount: interactor.getCartCount,()
        ))
        
        let tabBarController    = GroceryTabBarViewController(tabs: tabs, presenter: presenter)
        presenter.view          = tabBarController
        
        tabBarController.tabBar.tintColor = UIColor.primary
        return tabBarController
    }
}
