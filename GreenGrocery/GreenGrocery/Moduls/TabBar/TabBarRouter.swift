//
//  TabBarRouter.swift
//  GreenGrocery
//
//  Created by bekbossynova on 8/7/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

class TabBarRouter {
    
    var viewController: UIViewController
    
    typealias Submodules = (
        home: UIViewController,
        cart: UIViewController,
        profile: UIViewController
    )
    
    init(viewController: UIViewController, subModuls: Submodules) {
        self.viewController = viewController
    }
}


extension TabBarRouter {
    static func tabs(usingSubModules submoduls: Submodules) -> GroceryTabs {
        let homeTabBarItem  = UITabBarItem(title: "Home", image: UIImage(named: "home"), tag: 11)
        let cartTabBarItem  = UITabBarItem(title: "Cart", image: UIImage(named: "cart"), tag: 12)
        let profileTabBarItem  = UITabBarItem(title: "Profile", image: UIImage(named: "profile"), tag: 13)
        submoduls.home.tabBarItem = homeTabBarItem
        submoduls.cart.tabBarItem = cartTabBarItem
        submoduls.profile.tabBarItem = profileTabBarItem
        return (
            home: submoduls.home,
            cart: submoduls.cart,
            profile: submoduls.profile
        )
    }
}
