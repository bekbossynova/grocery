//
//  GroceryTabBarViewController.swift
//  GreenGrocery
//
//  Created by bekbossynova on 8/7/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

typealias GroceryTabs = (
    home: UIViewController,
    cart: UIViewController,
    profile: UIViewController
)

protocol TabBarView: class {
    func updateCartCount(count: Int) -> Void
}

class GroceryTabBarViewController: UITabBarController {
    
    var cartTab: UIViewController
    var presenter: TabBarPresentation?
    
    init(tabs: GroceryTabs, presenter: TabBarPresentation) {
        self.presenter = presenter
        self.cartTab = tabs.cart
        super.init(nibName: nil, bundle: nil)
        viewControllers = [ tabs.home, tabs.cart, tabs.profile]
       

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter?.viewDidLoad()

      
    }
}


extension GroceryTabBarViewController: TabBarView {
    
    func updateCartCount(count: Int) {
        self.cartTab.tabBarItem.badgeValue = count > 0 ? "\(count)" : nil
    }
    
    
}
