//
//  CartModulBuilder.swift
//  GreenGrocery
//
//  Created by bekbossynova on 8/7/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

class CartModulBuilder {
    
   static func build(usingNavigationFactory factory: NavigationFactory) -> UIViewController {
        let storyBoard = UIStoryboard.init(name: "Cart", bundle: nil)
        let  view      = storyBoard.instantiateViewController(identifier: "CartViewController") as! CartViewController
            view.title  = "Cart"
        
        
        return factory(view)
    }
}
