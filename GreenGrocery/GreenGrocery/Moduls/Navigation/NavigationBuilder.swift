//
//  NavigationBuilder.swift
//  GreenGrocery
//
//  Created by bekbossynova on 8/6/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

typealias NavigationFactory = (UIViewController) -> (UINavigationController)

class NavigationBuilder {
    
    static func build(rootView: UIViewController) -> UINavigationController {
        let textStyleAttributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor.white,
            .font: UIFont.systemFont(ofSize: 22, weight: .medium)
        ]
        
        let largeTextStyleAttributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor.white,
            .font: UIFont.systemFont(ofSize: 28, weight: .medium)
        ]
        
        
        let navigationController = UINavigationController(rootViewController: rootView)
        
        navigationController.navigationBar.barTintColor     = UIColor.primary
        navigationController.navigationBar.backgroundColor  = UIColor.primary
        navigationController.navigationBar.tintColor        = UIColor.white
        navigationController.navigationBar.titleTextAttributes = textStyleAttributes
        navigationController.navigationBar.largeTitleTextAttributes = largeTextStyleAttributes
        navigationController.navigationBar.isTranslucent    = false
        navigationController.navigationBar.prefersLargeTitles = true
       
        return navigationController
    }
}
