//
//  ProfileModuleBuilder.swift
//  GreenGrocery
//
//  Created by bekbossynova on 8/7/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

class ProfileModuleBuilder {
    
    static func build(usingNavigationFactory factory: NavigationFactory) -> UIViewController {
        let storyBoard = UIStoryboard.init(name: "Profile", bundle: nil)
        let  view      = storyBoard.instantiateViewController(identifier: "ProfileViewController") as! ProfileViewController
          view.title  = "Profile"
        
        
        return factory(view)
    }
}


