//
//  GroceryResult.swift
//  GreenGrocery
//
//  Created by bekbossynova on 8/5/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

struct GroceryResult: Codable {
    let groceries: [Grocery]
    
}

struct Grocery: Codable {
    let skuId: String
    let title: String
    let image: String
    let details: String
    let price: Double
    
    private enum CodingKeys: String, CodingKey {
        case skuId = "sku-id"
        case title
        case image = "product_image"
        case details
        case price 
        
    }
    
}
