//
//  CartItem.swift
//  GreenGrocery
//
//  Created by bekbossynova on 8/6/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

struct CartItem {
    
    let skuId: String
    let value: Int
    
    init(skuId: String, value: Int) {
        self.skuId = skuId
        self.value = value
    }
}
  
