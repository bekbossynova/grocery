//
//  StubService.swift
//  GreenGrocery
//
//  Created by bekbossynova on 8/5/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import Foundation

typealias GroceriesClosure = (GroceryResult) -> (Void)

protocol GroceriesApi {
    func fetchGroceries( completion: GroceriesClosure) -> (Void)
}


class StubService {
    
    static let shared : StubService = StubService()
    private init() {
        
    }
}


extension StubService: GroceriesApi {
    
    func fetchGroceries(completion: GroceriesClosure) {
        if let url = Bundle.main.url(forResource: "products", withExtension: "json") {
           
            do {
                let data = try Data(contentsOf: url)
                let groceryResul = try JSONDecoder().decode(GroceryResult.self, from: data)
                completion(groceryResul)
                
                
            } catch {
                print("Something went wrong \(error.localizedDescription)")
            }
        }
    }
}
