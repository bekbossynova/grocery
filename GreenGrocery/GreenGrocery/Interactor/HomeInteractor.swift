//
//  HomeInteractor.swift
//  GreenGrossery
//
//  Created by bekbossynova on 8/3/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import Foundation
import RealmSwift

typealias SkuItem = (skuId: String, quantity: Int)

class HomeInteractor {

    var service:  GroceriesApi
    var database: CartDB
    
    
    init(service: GroceriesApi, database: CartDB) {
        self.service    = service
        self.database   = database
    }

}

extension HomeInteractor {
    
    func getGroceries(completion: (GroceryResult) -> (Void)) {
        self.service.fetchGroceries { (result) -> (Void) in
            completion(result)
            
        }
    }
}

