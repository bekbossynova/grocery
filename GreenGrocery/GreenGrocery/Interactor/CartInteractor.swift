//
//  CartInteractor.swift
//  GreenGrocery
//
//  Created by bekbossynova on 8/10/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit


class CartInteractor {
    var database: CartDB
    
    init(database: CartDB) {
        self.database = database
        
    }

}

extension CartInteractor {
    
    func getCartCount(closure: @escaping CartCountClosure) {
        self.database.getCount(closure: closure)
    }
    
    func getCartItem(skuId: String) -> CartItem {
        return self.database.getCartItem(usinSkuId: skuId)
    }
    
    func addToCart(skuiItem: SkuItem) -> Bool {
           guard skuiItem.quantity > 0 else {
               return self.database.delete(usingSkuId: skuiItem.skuId)
           }
          return self.database.updateCart(using: CartItem.init(skuId: skuiItem.skuId, value: skuiItem.quantity))
        
       }
}
